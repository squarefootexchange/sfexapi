﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SquareFootApi.Models
{
    public class auth
    {

        public Result createuser(SquareFootApi.tbl_usr param)
        {
            int status = 1;
            var number = GenerateRandomNo();
            var hash = SecurePasswordHasher.Hash(param.pswd);
            directsq_mainsfexdbEntities db = new directsq_mainsfexdbEntities();
            try
            {
                DateTime dateTime = DateTime.UtcNow.Date;
                //creating object for tbl_usr
                var usr = new tbl_usr
                {
                    created_on = dateTime,
                    fname = param.fname,
                    lname = param.lname,
                    email = param.email,
                    dob = param.dob,
                    contact = param.contact,
                    cnic_pass = param.cnic_pass,
                    ntn = param.ntn,
                    country = param.country,
                    city = param.city,
                    address = param.address,
                    status_id = status
                };
                db.tbl_usr.Add(usr);
                db.SaveChanges();

                string mailmatch = usr.email;
                
                //creating object for tbl_auth
                var au = new tbl_auth
                {
                    
                    usr_id = db.tbl_usr.Where(x => x.email == mailmatch).Select(x => x.usr_id).FirstOrDefault(),
                    email = param.email,
                    pswd = hash,
                    status_id = status,
                    role_id = 1,
                    otp_pass = number.ToString(),
                    created_on = dateTime
                };
                db.tbl_auth.Add(au);
                db.SaveChanges();
                
                return new Result { status = "true", message = "record inserted!", data = { } };
            }
            catch (Exception ex)
            {
               
                return new Result { status = "false", message ="record insertion failed!", data = { } };
            }
        }

        public int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
    }
}