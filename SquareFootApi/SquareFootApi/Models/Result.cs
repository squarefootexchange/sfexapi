﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SquareFootApi.Models
{
    public class Result
    {
        public string status { get; set; }
        public string message { get; set; }
        public object data{ get; set; }
        //public string excepetion { get; set; }
    }
}