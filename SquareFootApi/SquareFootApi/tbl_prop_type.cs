//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SquareFootApi
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_prop_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_prop_type()
        {
            this.tbl_main_property = new HashSet<tbl_main_property>();
        }
    
        public long prop_type_id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public System.DateTime created_on { get; set; }
        public System.DateTime updated_on { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_main_property> tbl_main_property { get; set; }
    }
}
