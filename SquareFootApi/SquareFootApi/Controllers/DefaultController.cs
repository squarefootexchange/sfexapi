﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Mail;
using Microsoft.Ajax.Utilities;
using SquareFootApi.Models;

namespace SquareFootApi.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Default/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Default

        //public void CreateUser(tbl_usr usr)
        //{
        //    try
        //    {
        //        var code = new RandomGenerator();
        //        SendEmail(usr.email, usr.fname, usr.lname);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
       
        [Route("api/auth/createuser")]
        [HttpPost]
        public Result CreateUser(tbl_usr usr)
        {
            auth si = new auth();
            return si.createuser(usr);
           
            
        }
        //public Result CreateUser1(tbl_usr usr)
        //{
        //    auth si = new auth();
        //    return si.createuser(usr);


        //}
        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
        }

        public static void SendEmail(string email, string fname, string lname)


      {

            //MailMessage message = new MailMessage();
            //SmtpClient smtp = new SmtpClient();
            //message.From = new MailAddress("FromMailAddress");
            //message.To.Add(new MailAddress("ToMailAddress"));
            //message.Subject = "Test";
            //message.IsBodyHtml = true; //to make message body as html  
            //message.Body = htmlString;
            //smtp.Port = 587;
            //smtp.Host = "smtp.gmail.com"; //for gmail host  
            //smtp.EnableSsl = true;
            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = new NetworkCredential("FromMailAddress", "password");
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //smtp.Send(message);



            MailMessage mail = new MailMessage();
            mail.To.Add(email);
            //mail.To.Add("Another Email ID where you wanna send same email");
            mail.From = new MailAddress(email);
            mail.Subject = "Email Verification";

            mail.Body = "Hey " + fname + " " + " " + " " + lname + " , <br/> <br/> Thank you for signing up at Square Foot Exchange Pro.Please <a href = \"http://portal.squarefootexchange.com/Signup/emailVerify?code=" + "\">Click here</a> to verify your account. <br/> Once you verify your email, we will verify your details and approve your account. <br/> Team Square Foot Exchange";

            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.squarefootexchange.com"; //Or Your SMTP Server Address
            smtp.Port = 25;
            smtp.Credentials = new System.Net.NetworkCredential
            ("noreply@squarefootexchange.com", "SFeX2019@*");
            //Or your Smtp Email ID and Password
            //smtp.EnableSsl = true;
            smtp.Send(mail);
        }
        public class RandomGenerator
        {
            private readonly Random _random = new Random();
            public int RandomNumber(int min, int max)
            {
                return _random.Next(min, max);
            }
        }
    }
}
